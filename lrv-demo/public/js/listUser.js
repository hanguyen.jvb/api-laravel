$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$(document).ready(function() {
    $('.create').click(function() {
        $('#create').modal('show');
        $('.done').click(function() {

            let name1 = $("#name").val();
            let email = $("#email").val();

            $.ajax({
                url: '/api/users',
                type: 'post',
                data : {
                    name: name1,
                    email: email,
                },

                dataType : 'json',
                success: function(data) {
                    if (data.result == 'true') {
                        alert('ok cmmd');
                        $('#create').modal('hide');
                        location.reload();
                    }
                },

            });
        });
    });

    $('.edit').click(function() {
        $("#edit").modal('show');
        var id = $(this).data('id');
        $.ajax({
            url: '/api/users/' + id + '/edit',
            dataType: 'json',
            type: 'get',
            success: function($data) {
                console.log($data);
                $('.editName').val($data.user.name);
                $('.eidtEmail').val($data.user.email);
            }
        });

        $('.edit-click').click(function() {
            let name = $('#editName').val();
            let email = $('#eidtEmail').val();
            $.ajax({
                url : '/api/users/' + id,
                dataType : 'json',
                data : {
                    name : name,
                    email: email,
                },
                type : 'put',
                success : function(data) {
                    alert('update ok cmnd');
                    $('#edit').modal('hide');
                    location.reload();
                }
            });
        });
    });

    $('.delete').click(function() {
        $('#delete').modal('show');
        let id = $(this).data('id');
        $('.del').click(function() {
            $.ajax({
                url : '/api/users/' + id,
                type : 'delete',
                dataType : 'json',
                success : function(data) {
                    alert('ok da xoa cmnr');
                    $("#delete").modal('hide');
                    location.reload();
                }
            });
        });
    });
});
