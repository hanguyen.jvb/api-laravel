<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.0/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.0/js/bootstrap.min.js"></script>
    <script src="{{ asset('js/listUser.js') }}"></script>

</head>
<body>
    <button class="btn btn-info create" type="button">add +</button>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">email</th>
            </tr>
        </thead>
        <tbody class="listUser">
            @foreach ($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td><button type="button" class="btn btn-warning edit" data-id="{{ $user->id }}">edit</button></td>
                <td><button type="button" class="btn btn-danger delete" data-id="{{ $user->id }}">delete</button></td>
            </tr>
            @endforeach
        </tbody>
      </table>

  <!-- Modal -->
    <div class="modal fade" id="create" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal create</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form method="post">
                    <div class="form-group">
                        <label>name</label>
                        <input type="text" class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label>email</label>
                        <input type="text" class="form-control" id="email">
                    </div>
                    <button type="button" class="btn btn-success done">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>

        </div>
        </div>
    </div>

    <div class="modal fade" id="edit">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal edit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form method="post">
                    <div class="form-group">
                        <label>name</label>
                        <input type="text" class="form-control editName" id="editName">
                    </div>
                    <div class="form-group">
                        <label>email</label>
                        <input type="text" class="form-control eidtEmail" id="eidtEmail">
                    </div>
                    <button type="button" class="btn btn-success edit-click">Save edit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </form>
            </div>

        </div>
        </div>
    </div>

    <div class="modal fade" id="delete">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">may muon xoa khong?</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-danger del">yes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">no</button>
            </div>
          </div>
        </div>
      </div>
</body>
</html>
